FROM maven:3-jdk-8-alpine
VOLUME /tmp
ADD ./target/api-system-0.0.2-SNAPSHOT.jar api-system.jar
ENTRYPOINT [ "java", "-jar", "/api-system.jar" ]
