package com.giflo.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.giflo.commons.models.dto.SuscripcionDTO;
import com.giflo.system.model.service.ISuscripcionService;

@RestController
public class SuscripcionController {

	@Autowired
	private ISuscripcionService iSuscripcionService;

	@PostMapping("/rest/suscripcion/register")
	public ResponseEntity<?> registerSubscription(@RequestBody SuscripcionDTO r) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(iSuscripcionService.registerSubscription(r));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/suscripcion/recover")
	public ResponseEntity<?> recoverSubscription(@RequestParam(value = "c") Integer idUsuario) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iSuscripcionService.recoverSubscription(idUsuario));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/suscripcion/valid")
	public ResponseEntity<?> validSubscription(@RequestParam(value = "c") Integer idUsuario) {
		try {
			return ResponseEntity.status(HttpStatus.ACCEPTED).body(iSuscripcionService.validSubscription(idUsuario));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/rest/suscripcion/state")
	public ResponseEntity<?> stateSubscription(@RequestBody SuscripcionDTO r) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(iSuscripcionService.stateSubscription(r));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@PostMapping("/rest/suscripcion/coupon")
	public ResponseEntity<?> couponApplied(@RequestParam(value = "c1") Integer idSuscripcion,
			@RequestParam(value = "c2") String codigoCupon) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED)
					.body(iSuscripcionService.couponApplied(idSuscripcion, codigoCupon));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

	@GetMapping("/rest/suscripcion/redeemHours")
	public ResponseEntity<?> cangeHoras(@RequestParam(value = "c") Integer idUsuario,
			@RequestParam(value = "c2") Integer horasCanje) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED)
					.body(iSuscripcionService.redeemHours(idUsuario, horasCanje));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.GONE).body(e.getMessage());
		}
	}

}
