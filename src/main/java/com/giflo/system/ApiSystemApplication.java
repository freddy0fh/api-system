package com.giflo.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
@EntityScan({"com.giflo.commons.models.entity"})
public class ApiSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiSystemApplication.class, args);
	}
	

}
