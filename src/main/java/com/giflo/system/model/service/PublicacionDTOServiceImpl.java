package com.giflo.system.model.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.giflo.commons.models.dto.PublicacionDTO;
import com.giflo.commons.models.entity.Catalogo;
import com.giflo.commons.models.entity.DetallePublicacion;
import com.giflo.commons.models.entity.Flor;
import com.giflo.commons.models.entity.Publicacion;
import com.giflo.commons.models.entity.Usuario;
import com.giflo.system.model.repository.CatalogoRepository;
import com.giflo.system.model.repository.DetallePublicacionRepository;
import com.giflo.system.model.repository.FlorRepository;
import com.giflo.system.model.repository.PublicacionRepository;

@Service
public class PublicacionDTOServiceImpl implements IPublicacionDTOService {

	@Autowired
	PublicacionRepository publicacionRepository;

	@Autowired
	DetallePublicacionRepository detallePublicacionRepository;

	@Autowired
	FlorRepository florRepository;

	@Autowired
	private CatalogoRepository catalogoRepository;

	@Autowired
	private IFirebaseMessageService firebaseMessageService;

	@Autowired
	private IUsuarioService iUsuarioService;

	@Override
	public Map<String, Object> findByFlorAndPublicacion(Integer idTipoPublicacion, Integer idColor, String search,
			Integer page, Integer size) {
		Pageable paging = PageRequest.of(page, size);
		search = search != null ? search.trim() : "";
		Page<Publicacion> padeRet = publicacionRepository.findByColorAndPublicacion(idTipoPublicacion, idColor,
				search != null ? search.trim() : "", Calendar.getInstance().getTime(), paging);
		List<PublicacionDTO> lista = padeRet.getContent().stream().map(p -> new PublicacionDTO(p))
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		response.put("data", lista);
		response.put("page", padeRet.getNumber());
		response.put("totalPages", padeRet.getTotalPages());
		response.put("count", padeRet.getTotalElements());
		return response;
	}

	@Override
	public Map<String, Object> findByFilter(String search, Integer page, Integer size) {
		Pageable paging = PageRequest.of(page, size);
		search = search != null ? search.trim() : "";
		Page<Publicacion> padeRet = publicacionRepository.findByVariedadAndEmpresa(search,
				Calendar.getInstance().getTime(), paging);
		List<PublicacionDTO> lista = padeRet.getContent().stream().map(p -> new PublicacionDTO(p))
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		response.put("data", lista);
		response.put("page", padeRet.getNumber());
		response.put("totalPages", padeRet.getTotalPages());
		response.put("count", padeRet.getTotalElements());
		return response;
	}

	@Override
	public List<PublicacionDTO> findByUsuarioVigente(Integer idUsuario) {
		return publicacionRepository.findByUsuarioVigente(idUsuario, Calendar.getInstance().getTime()).stream()
				.map(p -> new PublicacionDTO(p)).collect(Collectors.toList());
	}

	@Override
	public List<PublicacionDTO> findByEmpresaVigente(Integer idEmpresa, Integer idPublicacion) {
		return publicacionRepository.findByEmpresaVigente(idEmpresa, idPublicacion, Calendar.getInstance().getTime())
				.stream().map(p -> new PublicacionDTO(p)).collect(Collectors.toList());
	}

	@Override
	public PublicacionDTO crear(Publicacion p) throws Exception {
		if (p.getId() == -1) {
			Flor flor = florRepository.buscarPorId(p.getIdFlor());
			Catalogo tipoPub = catalogoRepository.findById(p.getIdTipoPublicacion()).orElse(null);
			Publicacion nuevo = new Publicacion();
			nuevo.setFlor(flor);
			nuevo.setEstado("ACTIVO");
			nuevo.setTieneBoton(p.getTieneBoton());
			nuevo.setTieneTallo(p.getTieneTallo());
			nuevo.setFechaPublicacion(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
			nuevo.setTipoPublicacion(tipoPub);
			nuevo.setInicioVigencia(p.getInicioVigencia());
			nuevo.setFinVigencia(p.getFinVigencia());
			publicacionRepository.save(nuevo);
			
			List<DetallePublicacion> detalles = new ArrayList<>(0);
			for (DetallePublicacion dt : p.getDetalles()) {
				DetallePublicacion nuevoDet = new DetallePublicacion();
				nuevoDet.setCantidad(dt.getCantidad());
				nuevoDet.setPrecio(dt.getPrecio());
				nuevoDet.setEstado("ACTIVO");
				nuevoDet.setLargoTallo("SI".equalsIgnoreCase(p.getTieneTallo())
						? catalogoRepository.findById(dt.getIdLargoTallo()).orElse(null)
						: null);
				nuevoDet.setTamanioBoton("SI".equalsIgnoreCase(p.getTieneBoton())
						? catalogoRepository.findById(dt.getIdTamanioBoton()).orElse(null)
						: null);
				nuevoDet.setPublicacion(nuevo);
				detallePublicacionRepository.save(nuevoDet);
				detalles.add(nuevoDet);
			}
			nuevo.setDetalles(detalles);
			List<Usuario> usuarioToken = iUsuarioService.findTokens(flor.getEmpresa().getIdUsuario());
			firebaseMessageService.sendPushPublicacion(nuevo, usuarioToken);
			return new PublicacionDTO(nuevo);
		} else {
			Publicacion nuevo = publicacionRepository.findById(p.getId()).orElse(null);
			Catalogo tipoPub = catalogoRepository.findById(p.getIdTipoPublicacion()).orElse(null);
			if (nuevo != null) {
				Flor flor = florRepository.findById(p.getIdFlor()).orElse(null);
				nuevo.setFlor(flor);
				nuevo.setEstado("ACTIVO");
				nuevo.setTipoPublicacion(tipoPub);
				nuevo.setFechaPublicacion(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
				nuevo.setInicioVigencia(p.getInicioVigencia());
				nuevo.setFinVigencia(p.getFinVigencia());
				publicacionRepository.save(nuevo);
				detallePublicacionRepository.deleteAll(nuevo.getDetalles());
				for (DetallePublicacion dt : p.getDetalles()) {
					DetallePublicacion nuevoDet = new DetallePublicacion();
					nuevoDet.setCantidad(dt.getCantidad());
					nuevoDet.setPrecio(dt.getPrecio());
					nuevoDet.setEstado("ACTIVO");
					nuevoDet.setLargoTallo("SI".equalsIgnoreCase(p.getTieneTallo())
							? catalogoRepository.findById(dt.getIdLargoTallo()).orElse(null)
							: null);
					nuevoDet.setTamanioBoton("SI".equalsIgnoreCase(p.getTieneBoton())
							? catalogoRepository.findById(dt.getIdTamanioBoton()).orElse(null)
							: null);
					nuevoDet.setPublicacion(nuevo);
					detallePublicacionRepository.save(nuevoDet);
				}
				nuevo = publicacionRepository.findById(p.getId()).orElse(null);
				firebaseMessageService.sendPushPublicacion(nuevo,
						iUsuarioService.findTokens(flor.getEmpresa().getUsuarios().get(0).getId()));
				return new PublicacionDTO(nuevo);
			}
			return null;
		}

	}

	@Override
	public Boolean activarInactivar(Integer id, PublicacionDTO p) {
		Publicacion upd = publicacionRepository.findById(id).orElse(null);
		if (upd != null) {
			upd.setEstado(p.getEstado());
			publicacionRepository.save(upd);
			return true;
		}
		return false;

	}

}
