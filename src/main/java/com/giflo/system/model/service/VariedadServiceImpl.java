package com.giflo.system.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.giflo.commons.models.entity.Catalogo;
import com.giflo.commons.models.entity.Variedad;
import com.giflo.system.model.repository.CatalogoRepository;
import com.giflo.system.model.repository.VariedadRepository;

@Service
public class VariedadServiceImpl implements IVariedadService {

	@Autowired
	private CatalogoRepository catalogoRepository;

	@Autowired
	private VariedadRepository variedadRepository;

	@Override
	public Variedad save(Variedad v) {
		Catalogo color = catalogoRepository.findById(v.getIdColor()).orElse(null);
		v.setColor(color);
		variedadRepository.save(v);
		return variedadRepository.findByIdCustom(v.getId());
	}

}
