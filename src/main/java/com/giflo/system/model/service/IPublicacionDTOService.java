package com.giflo.system.model.service;

import java.util.List;
import java.util.Map;

import com.giflo.commons.models.dto.PublicacionDTO;
import com.giflo.commons.models.entity.Publicacion;

public interface IPublicacionDTOService {

	/**
	 * Obtiene las publicaciones de una empresa
	 * 
	 * @param idEmpresa
	 * @return
	 */
	List<PublicacionDTO> findByEmpresaVigente(Integer idEmpresa, Integer idPublicacion);

	/**
	 * Crea una neuva publicacion
	 * 
	 * @param p
	 * @return
	 * @throws Exception
	 */
	PublicacionDTO crear(Publicacion p) throws Exception;

	/**
	 * 
	 * @param id
	 * @param p
	 * @return
	 */
	Boolean activarInactivar(Integer id, PublicacionDTO p);

	Map<String, Object> findByFlorAndPublicacion(Integer idTipoPublicacion, Integer idColor, String search,
			Integer page, Integer size);

	/**
	 * 
	 * @param idUsuario
	 * @return
	 */
	List<PublicacionDTO> findByUsuarioVigente(Integer idUsuario);

	/**
	 * Busca publicaciones por paginación
	 * @param search
	 * @param page
	 * @param size
	 * @return
	 */
	Map<String, Object> findByFilter(String search, Integer page, Integer size);
}
