package com.giflo.system.model.service;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.giflo.commons.models.dto.MessageDTO;
import com.giflo.commons.models.entity.Catalogo;
import com.giflo.commons.models.entity.Empresa;
import com.giflo.commons.models.entity.Flor;
import com.giflo.commons.models.entity.Publicacion;
import com.giflo.commons.models.entity.TokenFcm;
import com.giflo.commons.models.entity.Usuario;
import com.giflo.commons.models.entity.Variedad;
import com.giflo.system.model.repository.UsuarioRepository;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;

@Service
public class FirebaseMessageServiceImpl implements IFirebaseMessageService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	private Logger log = LoggerFactory.getLogger(FirebaseMessageServiceImpl.class);

	@Autowired
	private Environment env;
	private final Integer cantidadMaxMensajes = 2;

	public FirebaseMessageServiceImpl() {
	}

	@PostConstruct
	void init() {
		try {
			String nameClient = env.getProperty("firebase.sdk.secret", String.class);
			InputStream resourceAsStream = FirebaseMessageServiceImpl.class.getClassLoader()
					.getResourceAsStream(nameClient);
			GoogleCredentials credential = GoogleCredentials.fromStream(resourceAsStream);
			FirebaseOptions options = FirebaseOptions.builder().setCredentials(credential).build();
			FirebaseApp.initializeApp(options);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	@Override
	public void sendPushPublicacion(Publicacion p, List<Usuario> usuarios) {
		try {
			if (usuarios.size() > 0) {
				String pattern = "EEEEE MMMMM yyyy";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("es", "ES"));
				String vigenteHasta = simpleDateFormat.format(p.getFinVigencia());
				Flor flor = p.getFlor();
				Variedad variedad = flor.getVariedad();
				Empresa empresa = flor.getEmpresa();
				Catalogo tp = p.getTipoPublicacion();
				HashMap<Integer, List<Message>> mapToken = new HashMap<>();
				Integer indice = 0;
				Integer contador = 0;
				List<Message> lista = new ArrayList<>(0);
				for (Usuario u : usuarios) {
					for (TokenFcm fcm : u.getTokenFcms()) {
						if (contador < cantidadMaxMensajes) {

							HashMap<String, String> map = new HashMap<>();
							map.put("idPublicacion", p.getId().toString());
							map.put("tipoPublicacion", p.getTipoPublicacion().getCodigo());
							map.put("tipoPush", "PUB");
							Message m = Message.builder().putAllData(map).setToken(fcm.getToken())
									.setNotification(Notification.builder()
											.setTitle(String.format("!Hola %1s, %2s creo una nueva publicación¡",
													u.getNombres(), empresa.getNombre()))
											.setBody(String.format("Variedad %1s, %2s vigente hasta %3s",
													variedad.getNombre(), tp.getNombre(), vigenteHasta))
											.setImage(variedad.getFotoPequenia()).build())
									.build();
							lista.add(m);
							contador++;
						} else {
							mapToken.put(indice, lista);
							contador = 0;
							lista = new ArrayList<>(0);
							indice++;
						}
					}
				}
				if (contador != 0 && contador < cantidadMaxMensajes) {
					mapToken.put(indice, lista);
				}
				Set<Integer> keys = mapToken.keySet();
				for (Integer key : keys) {
					List<Message> messages = mapToken.get(key);
					BatchResponse response = FirebaseMessaging.getInstance().sendAll(messages);// sendMulticast(message);
					if (response.getFailureCount() > 0) {
						log.warn(String.format("No se envio los mensaje a %1S dispositivos",
								response.getFailureCount()));

					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public MessageDTO sendPushMessage(MessageDTO dto) throws FirebaseMessagingException {

		Iterable<Usuario> usuarios = usuarioRepository.findAll();
		List<Message> lista = new ArrayList<>(0);
		HashMap<Integer, List<Message>> mapToken = new HashMap<>();
		Integer indice = 0;
		Integer contador = 0;
		for (Usuario u : usuarios) {
			for (TokenFcm fcm : u.getTokenFcms()) {
				if (contador < cantidadMaxMensajes) {
					HashMap<String, String> map = new HashMap<>();
					map.put("tipoPush", "NOT");
					map.put("urlImagePub", dto.getUrlImagePub());
					map.put("url", dto.getUrl());
					map.put("labelBoton", dto.getLabelBoton());
					Message m = Message.builder().putAllData(map).setToken(fcm.getToken())
							.setNotification(Notification.builder()
									.setTitle(String.format("!Hola %1s, %2s", u.getNombres(), dto.getTitle()))
									.setBody(dto.getBody()).setImage(dto.getUrlImage()).build())
							.build();
					lista.add(m);
					contador++;
				} else {
					mapToken.put(indice, lista);
					contador = 0;
					lista = new ArrayList<>(0);
					indice++;
				}
			}
		}
		if (contador != 0 && contador < cantidadMaxMensajes) {
			mapToken.put(indice, lista);
		}
		Set<Integer> keys = mapToken.keySet();
		for (Integer key : keys) {
			List<Message> messages = mapToken.get(key);
			BatchResponse response = FirebaseMessaging.getInstance().sendAll(messages);// sendMulticast(message);
			if (response.getFailureCount() > 0) {
				log.warn(String.format("No se envio los mensaje a %1S dispositivos", response.getFailureCount()));
			}
		}
		dto.setTitle("Publicado Exitosamente");
		return dto;

	}

}
