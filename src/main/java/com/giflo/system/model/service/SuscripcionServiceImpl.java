package com.giflo.system.model.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import com.giflo.commons.models.dto.CuponDTO;
import com.giflo.commons.models.dto.SuscripcionDTO;
import com.giflo.commons.models.entity.Cupon;
import com.giflo.commons.models.entity.Parametro;
import com.giflo.commons.models.entity.Plan;
import com.giflo.commons.models.entity.Suscripcion;
import com.giflo.commons.models.entity.Usuario;
import com.giflo.commons.models.enums.ValorEstadoSuscripcionEnum;
import com.giflo.system.model.repository.CatalogoRepository;
import com.giflo.system.model.repository.CuponRepository;
import com.giflo.system.model.repository.ParametroRepository;
import com.giflo.system.model.repository.PlanRepository;
import com.giflo.system.model.repository.SuscripcionRepository;
import com.giflo.system.model.repository.UsuarioRepository;

@Service
public class SuscripcionServiceImpl implements ISuscripcionService {

	@Autowired
	private SuscripcionRepository suscripcionRepository;

	@Autowired
	PlanRepository planRepository;

	@Autowired
	CatalogoRepository catalogoRepository;

	@Autowired
	CuponRepository cuponRepository;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	private ParametroRepository parametroRepository;
	
	private Logger log = LoggerFactory.getLogger(SuscripcionServiceImpl.class);

	@Override
	public SuscripcionDTO registerSubscription(SuscripcionDTO register) throws Exception {

		SuscripcionDTO ret = new SuscripcionDTO();
		Plan pla = planRepository.findById(register.getIdPlan()).orElse(null);
		Usuario user = usuarioRepository.findById(register.getIdUsuario()).orElse(null);
		Cupon cupon = null;
		BigDecimal descuento = BigDecimal.valueOf(0.0);
		BigDecimal subtotal = BigDecimal.valueOf(0.0);
		BigDecimal iva = BigDecimal.valueOf(0.0);
		BigDecimal total = BigDecimal.valueOf(0.0);
		try {
			cupon = cuponRepository.findByCodigoAndPlan(register.getCupon(), register.getIdPlan());
		} catch (Exception e) {
			cupon = null;
		}
		Parametro ivaParam = null;
		try {
			ivaParam = parametroRepository.buscarPorId("SUS_IVA");
		} catch (Exception e) {
			ivaParam = null;
		}
		if (pla != null && user != null) {
			String code = generateCodeSuscription().toUpperCase();
			Calendar fechaInicio = GregorianCalendar.getInstance();
			List<Suscripcion> lista = suscripcionRepository.findVigenteByIdUsuarioLast(register.getIdUsuario());
			if (!lista.isEmpty()) {
				fechaInicio.setTime(lista.get(0).getVigenciaFin());
				fechaInicio.add(Calendar.DAY_OF_YEAR, 1);
			}

			Calendar fechaFin = GregorianCalendar.getInstance();
			fechaFin.add(Calendar.MONTH, pla.getMeses());
			Suscripcion sus = new Suscripcion();
			sus.setPlan(pla);
			sus.setUsuario(user);
			sus.setCupon(register.getCupon());
			sus.setEstado(register.getEstado() != null ? register.getEstado() : ValorEstadoSuscripcionEnum.PENDIENTE);
			total = pla.getValor();
			subtotal = total;
			if (cupon != null) {
				descuento = total.multiply(cupon.getPorciento().divide(BigDecimal.valueOf(100)));
				subtotal = total.subtract(descuento);
			} else {

			}
			if (ivaParam != null) {
				iva = subtotal.multiply(BigDecimal.valueOf(Double.parseDouble(ivaParam.getValor())));
			}
			total = subtotal.add(iva);
			sus.setCodigoPago(code);
			sus.setDescuento(descuento);
			sus.setSubtotal(subtotal);
			sus.setIva(iva);
			sus.setTotal(total);
			sus.setVigenciaInicio(new Timestamp(fechaInicio.getTimeInMillis()));
			sus.setVigenciaFin(new Timestamp(fechaFin.getTimeInMillis()));
			sus.setFechaRegistro(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
			sus.setFechaPago(register.getFechaPago());
			suscripcionRepository.save(sus);
			ret.setCodigoPago(code);
		}
		return ret;
	}

	@Override
	public SuscripcionDTO redeemHours(Integer idUser, Integer horasCanjeUsuario) throws Exception {

		SuscripcionDTO ret = new SuscripcionDTO();
		Parametro paramPlan = parametroRepository.buscarPorId("SUS_PLAN_HORAS");
		Parametro paramPlanMult = parametroRepository.buscarPorId("SUS_PLAN_HORAS_MULTIPLO");
		if (paramPlanMult == null) {
			ret.setId(-1);
			ret.setMensaje("No se ha definido el parámetro plan de multiplos para los canjes");
			return ret;
		}

		if (paramPlan == null) {
			ret.setId(-1);
			ret.setMensaje("No se ha definido el parámetro plan para los canjes");
			return ret;
		}
		Plan pla = planRepository.findById(Integer.parseInt(paramPlan.getValor())).orElse(null);
		if (pla == null) {
			ret.setId(-1);
			ret.setMensaje("No se ha definido el plan para los canjes");
			return ret;
		}
		Usuario user = usuarioRepository.findById(idUser).orElse(null);
		// Verificamos que sea multiplo de días
		Integer multiplo = Integer.parseInt(paramPlanMult.getValor());
		if (user.getHorasAcomuladas() < multiplo) {
			ret.setId(-1);
			ret.setMensaje(String.format("La cantidad de horas acomuladas debe ser mayor o igual a %1s", multiplo));
			return ret;
		}
		if (horasCanjeUsuario > user.getHorasAcomuladas()) {
			ret.setId(-1);
			ret.setMensaje("La cantidad de horas acomuladas supera a las que el usuario posee");
			return ret;
		}
		Integer resto = user.getHorasAcomuladas() - horasCanjeUsuario;
		Integer diasCanje = horasCanjeUsuario / multiplo;
		BigDecimal descuento = BigDecimal.valueOf(0.0);
		BigDecimal subtotal = BigDecimal.valueOf(0.0);
		BigDecimal iva = BigDecimal.valueOf(0.0);
		BigDecimal total = BigDecimal.valueOf(0.0);

		if (pla != null && user != null) {
			List<Suscripcion> lista = suscripcionRepository.findVigenteByIdUsuarioLast(idUser);
			Calendar fechaInicio = GregorianCalendar.getInstance();
			if (!lista.isEmpty()) {
				fechaInicio.setTime(lista.get(0).getVigenciaFin());
				fechaInicio.add(Calendar.DAY_OF_YEAR, 1);
			}
			String code = generateCodeSuscription().toUpperCase();
			Calendar fechaFin = GregorianCalendar.getInstance();
			fechaFin.setTime(fechaInicio.getTime());
			fechaFin.add(Calendar.DAY_OF_YEAR, diasCanje);
			Suscripcion sus = new Suscripcion();
			sus.setPlan(pla);
			sus.setUsuario(user);
			sus.setEstado(ValorEstadoSuscripcionEnum.VIGENTE);
			sus.setCodigoPago(code);
			sus.setDescuento(descuento);
			sus.setSubtotal(subtotal);
			sus.setIva(iva);
			sus.setTotal(total);
			sus.setVigenciaInicio(new Timestamp(fechaInicio.getTimeInMillis()));
			sus.setVigenciaFin(new Timestamp(fechaFin.getTimeInMillis()));
			sus.setFechaRegistro(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
			sus.setFechaPago(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
			suscripcionRepository.save(sus);
			user.setHorasAcomuladas(resto);
			usuarioRepository.save(user);
			ret.setHorasDisponibles(resto);
			ret.setHorasTomadas(horasCanjeUsuario);
			ret.setCodigoPago(code);
			ret.setId(sus.getId());
		}
		return ret;
	}

	@Transactional
	@Override
	public List<SuscripcionDTO> recoverSubscription(Integer idUsuario) throws Exception {
		List<Suscripcion> lista = suscripcionRepository.findByIdUsuario(idUsuario);
		List<SuscripcionDTO> suscripcionLista = new ArrayList<>(0);
		if (lista != null) {
			for (Suscripcion suscripcion : lista) {
				Cupon cupon = cuponRepository.findByCodigo(suscripcion.getCupon());
				SuscripcionDTO obj;
				if (cupon != null) {
					obj = new SuscripcionDTO(suscripcion, cupon);
				} else {
					obj = new SuscripcionDTO(suscripcion);
				}
				obj.setFechaActual(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
				suscripcionLista.add(obj);
			}
		}
		return suscripcionLista;
	}

	@Transactional
	@Override
	public List<SuscripcionDTO> validSubscription(Integer idUsuario) throws Exception {
		List<Suscripcion> lista = suscripcionRepository.findVigenteByIdUsuario(idUsuario,
				GregorianCalendar.getInstance().getTime());
		List<SuscripcionDTO> suscripcionLista = new ArrayList<>(0);
		if (lista != null) {
			for (Suscripcion suscripcion : lista) {
				Cupon cupon = cuponRepository.findByCodigo(suscripcion.getCupon());
				SuscripcionDTO obj;
				if (cupon != null) {
					obj = new SuscripcionDTO(suscripcion, cupon);
				} else {
					obj = new SuscripcionDTO(suscripcion);
				}
				obj.setFechaActual(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
				suscripcionLista.add(obj);
			}
		}
		return suscripcionLista;
	}

	@Transactional
	@Override
	public SuscripcionDTO stateSubscription(@RequestBody SuscripcionDTO r) throws Exception {
		Suscripcion sus = suscripcionRepository.findByIdSuscripcion(r.getId());
		if (sus != null) {
			if(ValorEstadoSuscripcionEnum.VIGENTE.equals(r.getEstado())) {
				List<Suscripcion> lista = suscripcionRepository.findVigenteByIdUsuarioLast(sus.getUsuario().getId());
				Calendar fechaInicio = GregorianCalendar.getInstance();
				if (!lista.isEmpty()) {
					fechaInicio.setTime(lista.get(0).getVigenciaFin());
					fechaInicio.add(Calendar.MONTH, 1);
				}
				Calendar fechaFin = GregorianCalendar.getInstance();
				fechaFin.setTime(fechaInicio.getTime());
				fechaFin.add(Calendar.MONTH, sus.getPlan().getMeses());
				sus.setVigenciaInicio(new Timestamp(fechaInicio.getTimeInMillis()));
				sus.setVigenciaFin(new Timestamp(fechaFin.getTimeInMillis()));
				sus.setEstado(r.getEstado());
				sus.setDocumento(r.getDocumento());
				sus.setFechaPago(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));	
			}else if(ValorEstadoSuscripcionEnum.PENDIENTE.equals(r.getEstado())) {
				sus.setEstado(r.getEstado());
				sus.setDocumento(null);
				sus.setFechaPago(null);
				sus.setVigenciaInicio(null);
				sus.setVigenciaFin(null);
			}else if(ValorEstadoSuscripcionEnum.CADUCADO.equals(r.getEstado())) {
				sus.setEstado(r.getEstado());
			}
			suscripcionRepository.save(sus);
		}
		return new SuscripcionDTO(sus);

	}

	@Transactional
	@Override
	public CuponDTO couponApplied(Integer idPLan, String codigoCupon) throws Exception {
		CuponDTO ret = new CuponDTO();
		ret.setIdCupon(-1);
		ret.setDescuento(BigDecimal.valueOf(0.0));
		ret.setSubtotal(BigDecimal.valueOf(0.0));
		ret.setIva(BigDecimal.valueOf(0.0));
		Plan plan = planRepository.findById(idPLan).orElse(null);
		if (plan != null) {
			ret.setTotal(plan.getValor());
			Cupon cupon = cuponRepository.findByCodigo(codigoCupon);
			if (cupon != null) {
				Parametro ivaParam = null;
				try {
					ivaParam = parametroRepository.buscarPorId("SUS_IVA");
				} catch (Exception e) {
					ivaParam = null;
				}
				if (cupon.getIdPlan() == plan.getId()) {
					BigDecimal valor_iva = BigDecimal.valueOf(0.0);
					BigDecimal porcentaje_iva = BigDecimal.valueOf(0.0);
					if (ivaParam != null) {
						valor_iva = BigDecimal.valueOf(Double.parseDouble(ivaParam.getValor()));
						porcentaje_iva = valor_iva.divide(new BigDecimal(100.00));
					}

					BigDecimal porcentaje = cupon.getPorciento().divide(new BigDecimal(100.00));
					BigDecimal descuento = plan.getValor().multiply(porcentaje);
					BigDecimal total = plan.getValor();
					BigDecimal subtotal = total.subtract(descuento);
					BigDecimal iva = subtotal.multiply(porcentaje_iva);
					String message = cupon.getMensaje().replace("[DESCUENTO]", descuento.toString());
					message = message.replace("[PORCENTAJE]", cupon.getPorciento().toString());
					total = subtotal.add(iva);
					ret.setMensaje(message);
					ret.setIdCupon(cupon.getId());
					ret.setDescuento(descuento);
					ret.setSubtotal(subtotal);
					ret.setIva(iva);
					ret.setTotal(total);
				} else {
					ret.setIdCupon(-2);
					ret.setMensaje("El cupon no es aplicable para el plan seleccionado.");
				}
			} else {
				ret.setIdCupon(-1);
				ret.setMensaje("El cupon no es válido");
			}

		}
		return ret;
	}

	private String generateCodeSuscription() {
		String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
		String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
		String numbers = RandomStringUtils.randomNumeric(3);
		String totalChars = RandomStringUtils.randomAlphanumeric(1);
		String combinedChars = upperCaseLetters.concat(lowerCaseLetters).concat(numbers).concat(totalChars);
		List<Character> pwdChars = combinedChars.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
		Collections.shuffle(pwdChars);
		String password = pwdChars.stream().collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
				.toString();
		return password;
	}

	private static final SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	@Override
	public void verifiedSuscriptions() {
		log.info(String.format("Se ejecuta el cron a las %1s", sdf3.format(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()))));
		List<Suscripcion> list=suscripcionRepository.findCaducated(GregorianCalendar.getInstance().getTime());
		for (Suscripcion suscripcion : list) {
			suscripcion.setEstado(ValorEstadoSuscripcionEnum.CADUCADO);
			suscripcionRepository.save(suscripcion);
		}
	}

}
