package com.giflo.system.model.service;

import java.util.List;

import com.giflo.commons.models.dto.MessageDTO;
import com.giflo.commons.models.entity.Publicacion;
import com.giflo.commons.models.entity.Usuario;
import com.google.firebase.messaging.FirebaseMessagingException;

public interface IFirebaseMessageService {

	/**
	 * Crea una notificación push
	 * 
	 * @param p
	 * @param token
	 */
	public void sendPushPublicacion(Publicacion p, List<Usuario> tokens);

	/**
	 * Envia una notificacion a todos los usuarios
	 * @param dto
	 * @throws FirebaseMessagingException 
	 */
	MessageDTO sendPushMessage(MessageDTO dto) throws FirebaseMessagingException;
}
