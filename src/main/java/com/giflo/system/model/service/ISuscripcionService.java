package com.giflo.system.model.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import com.giflo.commons.models.dto.CuponDTO;
import com.giflo.commons.models.dto.SuscripcionDTO;

public interface ISuscripcionService {
	
	/**
	 * 
	 * @param Registro 
	 * @return
	 * @throws Exception
	 */
	SuscripcionDTO registerSubscription(SuscripcionDTO register) throws Exception;
	
	/**
	 * 
	 * @param Recuperacion de datos
	 * @return
	 * @throws Exception
	 */
	List<SuscripcionDTO> recoverSubscription(Integer idUsuario) throws Exception;
	
	
	
	/**
	 * 
	 * @param Cupon aplicado
	 * @return
	 * @throws Exception
	 */
	CuponDTO couponApplied(Integer idPlan, String codigoCupon) throws Exception;
	
	/**
	 * 
	 * @param Cupones vigentes
	 * @return
	 * @throws Exception
	 */
	List<SuscripcionDTO> validSubscription(Integer idUsuario) throws Exception;
	/**
	 * 
	 * @param idUser
	 * @param horasCanjeUsuario
	 * @return
	 * @throws Exception
	 */
	SuscripcionDTO redeemHours(Integer idUser,Integer horasCanjeUsuario) throws Exception;


	/**
	 * 
	 * @param idSuscripcion Id de la suscripcion
	 * @param estado Nuevo Estado
	 * @param documento Documento de pago
	 * @return
	 * @throws Exception
	 */
	SuscripcionDTO stateSubscription(@RequestBody SuscripcionDTO r) throws Exception;

	/**
	 * Verifica las suscripciones vencidas
	 */
	void verifiedSuscriptions();

}
