package com.giflo.system.model.service;

import com.giflo.commons.models.dto.RespuestaOtpDTO;

public interface IOtpService {
	/**
	 * Envia un código a un email
	 * @param contacto
	 * @return
	 * @throws Exception 
	 */
	RespuestaOtpDTO enviarOTP(String contacto) throws Exception;

	/**
	 * Valida un codigo otp ingresado
	 * @param contacto
	 * @param codigo
	 * @return
	 */
	
	RespuestaOtpDTO validarOTP(String contacto, String codigo) throws Exception;

}
