package com.giflo.system.model.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.commons.models.entity.Direccion;
import com.giflo.commons.models.entity.Empresa;
import com.giflo.commons.models.enums.ValorSiNoEnum;

@RepositoryRestResource(path = "direccion")
public interface DireccionRepository extends PagingAndSortingRepository<Direccion, Integer> {

	@RestResource(path = "by-user")
	List<Direccion> findByEmpresaAndActivo(@Param(value = "q") Empresa empresa,@Param(value = "e") ValorSiNoEnum activo);
}
