package com.giflo.system.model.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.commons.models.entity.Suscripcion;
import com.giflo.commons.models.entity.Usuario;

@RepositoryRestResource(path = "suscripcion")
public interface SuscripcionRepository extends PagingAndSortingRepository<Suscripcion, Integer> {

	@RestResource(path = "by-usuario")
	List<Suscripcion> findByUsuario(Usuario usuario);

	@Query("select s from Suscripcion s where s.id=:idSuscripcion ")
	Suscripcion findByIdSuscripcion(@Param(value = "idSuscripcion") Integer idSuscripcion);

	@Query("select s from Suscripcion s where s.usuario.id=:idUsuario order by s.fechaRegistro desc ")
	List<Suscripcion> findByIdUsuario(@Param(value = "idUsuario") Integer idUsuario);

	@Query("select s from Suscripcion s where s.usuario.id=:idUsuario and s.estado=:estado order by s.fechaRegistro desc ")
	List<Suscripcion> findByUsuarioAndVigente(@Param(value = "idUsuario") Integer idUsuario,
			@Param(value = "estado") String estado);

	@Query("select s from Suscripcion s where s.usuario.id=:idUsuario and s.estado='VIGENTE' and :fecha <= s.vigenciaFin  ")
	List<Suscripcion> findVigenteByIdUsuario(@Param(value = "idUsuario") Integer idUsuario,
			@Param(value = "fecha") Date fecha);

	@Query("select s from Suscripcion s where (upper(s.usuario.correo)=upper(:filter) or upper(s.usuario.nombres)=upper(:filter) or upper(s.usuario.apellidos)=upper(:filter) or :filter='-1') and ( s.codigoPago=:codigoPago or :codigoPago='-1') order by s.fechaRegistro desc ")
	Page<Suscripcion> findByVariedadAndEmpresa(@Param(value = "filter") String filtro,
			@Param(value = "codigoPago") String codigoPago, Pageable pageable);
	
	@Query("select s from Suscripcion s where s.usuario.id=:idUsuario and s.estado='VIGENTE'   order by s.vigenciaFin desc")
	List<Suscripcion> findVigenteByIdUsuarioLast(@Param(value = "idUsuario") Integer idUsuario);
	
	@Query("select s from Suscripcion s where s.estado='VIGENTE' and  :fecha > s.vigenciaFin order by s.vigenciaFin desc")
	List<Suscripcion> findCaducated(@Param(value = "fecha") Date fecha);

}
