package com.giflo.system.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.commons.models.entity.Flor;

@RepositoryRestResource(path = "flor")
public interface FlorRepository extends PagingAndSortingRepository<Flor, Integer> {

	@Query("select new Flor(f.id,f.disponible,f.variedad.nombre,f.variedad.nombreFoto,f.variedad.indicaciones,f.variedad.fotoPequenia) from Flor f inner join f.empresa emp inner join emp.usuarios us  where us.id=:idUser and f.disponible='SI' order by f.variedad.nombre")
	List<Flor> findPorUsuario(@Param(value = "idUser") Integer idUser);

	@Query("select f from Flor f inner join f.empresa  emp inner join f.variedad v where emp.id=:idEmpresa and v.id=:idVariedad")
	List<Flor> validExist(@Param(value = "idEmpresa") Integer idEmpresa,
			@Param(value = "idVariedad") Integer idVariedad);

	@RestResource(path = "by-id")
	@Query("select new Flor(e.empresa,e) from Flor e where e.id=:id")
	Flor buscarPorId(@Param(value = "id") Integer id);

}
