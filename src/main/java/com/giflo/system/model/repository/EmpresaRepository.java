package com.giflo.system.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.commons.models.entity.Empresa;

@RepositoryRestResource(path = "empresa")
public interface EmpresaRepository extends PagingAndSortingRepository<Empresa, Integer> {

	@RestResource(path = "by-user")
	@Query("select e from Usuario u inner join u.empresa e where u.id=:id and e.activo=:estado")
	List<Empresa> findByUserAndActivo(@Param(value = "id") Integer id, @Param(value = "estado") String activo);

	@RestResource(path = "filter")
	List<Empresa> findByRucContaining(@Param(value = "q") String ruc);
	
	@RestResource(path = "by-id")
	@Query("select new Empresa(e) from Empresa e where e.id=:id")
	Empresa buscarPorId(@Param(value = "id") Integer id);
		
}
