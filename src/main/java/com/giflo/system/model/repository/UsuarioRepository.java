package com.giflo.system.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.giflo.commons.models.entity.Usuario;
import com.giflo.commons.models.enums.ValorSiNoEnum;

@RepositoryRestResource(path = "usuario")
public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Integer> {

	@RestResource(path = "by-username-active")
	Usuario findByUsernameAndActivo(@Param(value = "q") String username, @Param(value = "a") ValorSiNoEnum activo);

	@RestResource(path = "by-username")
	Usuario findByUsername(@Param(value = "username") String username);

	@RestResource(path = "by-correo")
	Usuario findByCorreo(@Param(value = "correo") String correo);

	@RestResource(path = "by-type-id-estado")
	List<Usuario> findByTipoIdentificacionAndIdentificacionAndActivo(@Param(value = "type") String type,
			@Param(value = "ide") String identification, @Param(value = "activo") ValorSiNoEnum activo);

	@Query("select new Usuario(us.id,us.identificacion,us.tipoIdentificacion,us.nombres,us.apellidos) from Usuario us  where us.username=:ide")
	List<Usuario> byTypeAndIde(@Param(value = "ide") String username);

	@Query("select us from Usuario us  where us.username=:ide")
	List<Usuario> byUsername(@Param(value = "ide") String username);

	@Query("select us from Usuario us  where us.username=:ide and us.recuperacion='SI'")
	List<Usuario> byUserRecuperacion(@Param(value = "ide") String username);

	@Query("select us from Usuario us inner join us.empresa emp  where us.id=:id  and us.activo='SI'")
	Usuario byIdCustom(@Param(value = "id") Integer id);

	@Query( value  = "select us.su_id from s_usuario us where us.su_id <> :idUser and us.su_activo='SI' and :idTipoEmpresa = ANY(us.su_not_tipo_empresa) and :idTipoPublicacion = ANY(us.su_not_tipo_publicacion)",nativeQuery = true)
	List<Object[]> findToken(@Param(value = "idUser") Integer idUser, @Param(value = "idTipoEmpresa") Integer idTipoEmpresa,@Param(value = "idTipoPublicacion") Integer idTipoPublicacion);
	
	@Query("select us from Usuario us inner join us.empresa emp  where emp.id=:idEmpresa")
	Usuario findByEmpresa(@Param(value = "idEmpresa") Integer id);
	
	@Query("select us from Usuario us inner join us.empresa emp  where us.activo='SI' and us.esAdmin='NO' and ( upper(us.nombres) like upper(:filter) or upper(us.apellidos) like upper(:filter) or upper(emp.nombre) like upper(:filter)) and us.id!=:idUser")
	List<Usuario> search(@Param(value = "filter") String filter,@Param(value = "idUser") Integer idUser );
}
