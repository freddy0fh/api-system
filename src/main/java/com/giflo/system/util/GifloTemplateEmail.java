package com.giflo.system.util;

import java.util.HashMap;

import com.giflo.commons.models.enums.TemplateEmailEnum;

public class GifloTemplateEmail {

	/**
	 * Encryption
	 *
	 * @param content
	 * @param key
	 * @return
	 */
	public static String buildEmail(HashMap<String, String> data, TemplateEmailEnum id) {
		StringBuilder b = new StringBuilder();
		switch (id) {
		case REGISTRO:
			String links = data.get("links");
			b.append("<h1>Bienvenido</h1>");
			b.append(String.format("<p>Estimado %1s tu registro se completo exitosamente</p>", data.get("usuario")));
			b.append("<p>Ahora ya puedes crear tus publicaciones y ponerte en contacto con otras empresas.</p>");
			if (!links.isEmpty()) {
				b.append("<br/>");
				b.append("<h4>Te sugerimos ver los siguientes videos de como usar el app.</h4>");
				String[] linksArray = links.split(",");
				for (int i = 0; i < linksArray.length; i++) {
					b.append(String.format("<p>%1s</p>", linksArray[i]));
				}
			}
			b.append("<br/>");
			b.append("<br/>");
			b.append("<br/>");
			b.append("<h3>Giflo</h3>");
			b.append("<p>Siempre Innovando</p>");
			break;
		case CAMBIO_CLAVE:
			b.append("<h1>Recuperación de Contraseña</h1>");
			b.append(String.format("<p>Estimado %1s</p>", data.get("usuario")));
			b.append(String.format(
					"<p>Para recuperar tu contraseña debes ingresar con la siguiente clave temporal <strong>%1s</strong> valida hasta %2s</p>",
					data.get("clave"), data.get("tiempo")));
			b.append("<br/>");
			b.append("<br/>");
			b.append("<br/>");
			b.append("<h3>Giflo</h3>");
			b.append("<p>Siempre Innovando</p>");
			break;
		case ACTUALIZACION:
			b.append("<h1>Actualización de Datos</h1>");
			b.append(
					String.format("<p>Estimado %1s se ha actualizado tus datos exitosamente</p>", data.get("usuario")));
			b.append("<p>Si tú no lo has realizado, comunícate con nosotros.</p>");
			b.append("<br/>");
			b.append("<br/>");
			b.append("<br/>");
			b.append("<h3>Giflo</h3>");
			b.append("<p>Siempre Innovando</p>");
			break;

		case ENVIO_OTP:
			b.append("<h1>Código de Verificación</h1>");
			b.append(String.format(
					"<p>Copie este código <strong style='font-size:20px;'>%1s</strong> e ingrese en la aplicación para continuar con su registro. Válida hasta %2s</p>",
					data.get("clave"), data.get("tiempo")));
			b.append("<p>Si tú no lo has realizado, comunícate con nosotros.</p>");
			b.append("<br/>");
			b.append("<br/>");
			b.append("<br/>");
			b.append("<h3>Giflo</h3>");
			b.append("<p>Siempre Innovando</p>");
			break;

		default:
			break;
		}
		return b.toString();

	}

}
