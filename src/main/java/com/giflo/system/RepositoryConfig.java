package com.giflo.system;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

import com.giflo.commons.models.entity.Catalogo;
import com.giflo.commons.models.entity.Cupon;
import com.giflo.commons.models.entity.Empresa;
import com.giflo.commons.models.entity.EmpresaFoto;
import com.giflo.commons.models.entity.Flor;
import com.giflo.commons.models.entity.Parametro;
import com.giflo.commons.models.entity.Perfil;
import com.giflo.commons.models.entity.Plan;
import com.giflo.commons.models.entity.Suscripcion;
import com.giflo.commons.models.entity.Usuario;
import com.giflo.commons.models.entity.Variedad;

@Configuration
public class RepositoryConfig implements RepositoryRestConfigurer {

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.exposeIdsFor(Empresa.class, Usuario.class, Perfil.class, Catalogo.class, Parametro.class, Variedad.class,
				Flor.class, Empresa.class, EmpresaFoto.class,Plan.class,Cupon.class,Suscripcion.class);
		config.getCorsRegistry().addMapping("/**").allowedOrigins("*").allowedHeaders("*")
				.allowedMethods("POST", "GET", "PUT", "DELETE").allowCredentials(true).maxAge(3600);
	}

}
